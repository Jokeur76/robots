var chars = {};
var compo = [];
var pass = "";

$(function() {
    $('#result').hide();
    raz();
    console.log('ready START pass puis chars');
    console.log(pass);
    console.log(chars);
    console.log('END pass puis chars');
});

function addCharType(type) {
    var id = createId(type);
    $('#selection').append('<div>Ajouter <input id="'+id+'" type="number" value="1" step="1" min="1" /> '+type+'</div>')
}

function validate() {
    console.log('validate START pass puis chars');
    console.log(pass);
    console.log(chars);
    console.log('END pass puis chars');
    var elt = {};
    $('#selection input').each(function(){
        elt = {};
        elt.type = $(this).attr('id');
        elt.qtt = $(this).val();
        compo.push(elt);
    });
    generatePass();
}

function generatePass() {

    var lengthCompo = compo.length;
    var lengthLoop = null;
    for(var i = 0; i < lengthCompo; i++){
        lengthLoop = compo[i].qtt;
        for(var j = 0; j < lengthLoop; j++){
            pass += getChar(compo[i].type);
            console.log(chars[compo[i].type])
        }
    }
    $('#pass').html(pass);
    $('#result').show();
    raz();

}

function getChar(type) {
    var max = chars[type].length;
    var ret = "";
    if(max > 0){
        var start = Math.floor(Math.random()*max);
        ret = chars[type][start];
        chars[type].splice(start, 1);
    } else {
        //alert('Vous avez requis trop de caractère du type : '+type+' en conséquence il ne sera pas ajouté de caractère de ce type');
    }
    return ret;

}

function raz() {
    chars = {};
    chars.Consonne = ["z","r","t","p","m","l","k","j","h","g","f","d","s","q","w","x","c","v","b","n","Z","R","T","P","M","L","K","J","H","G","F","D","S","Q","W","X","C","V","B","N"];
    chars.Voyelle = ["a","e","i","o","u","y","A","E","Y","O","U","Y"];
    chars.Nombre = ["0","1","2","3","4","5","6","7","8","9"];
    chars.Caracteres_speciaux = ["!","#","$","%","&","*","+","-","/","=","?","^","_","{","|","}"];
    compo = [];
    pass = "";
}



