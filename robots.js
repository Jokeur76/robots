var cheerio = require('cheerio');
var request = require('request');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var exphbs = require('express3-handlebars');
var path = require('path');
var port = 8080;


/** Configuration serveur */
app.use(express.static('assets'))
    // For Content-Type application/json
    .use(bodyParser.json())
    // For x-www-form-urlencoded
    .use(bodyParser.urlencoded({ extended: true }))
    .set('views', 'views/pages')
    .engine('handlebars', exphbs({defaultLayout: 'main'}))
    .set('view engine', 'handlebars');
exphbs.ExpressHandlebars.prototype.layoutsDir = 'views/';

/** Routage */
app.get('/', function (req, res){
        res.render('index');
    })
    .use(function(req, res){
        console.log('default gate');
        res.redirect('/');
    });

//socket
app.listen(port);




