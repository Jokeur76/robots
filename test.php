page index.php

<? /* 
--------------------------------------------------------
Information source initiale
Catégorie :Frames  
Classé sous :div, ajax, xmlhttprequest, onreadystatechange  
Niveau :Débutant  
Date de création :05/05/2006  
Date de mise à jour :05/05/2006 15:55:27
Auteur : atlante34
Description
permet de charger une page html dans un div
permet de ne pas utliser les frames 
-----------------------------------------------------------------------------

Information source modifier
Catégorie :Frames  
Classé sous :div, ajax, xmlhttprequest, onreadystatechange  
Niveau :Expert  
Date de création :05/05/2006  
Date de mise à jour :26/11/2009 9:25
Auteur : FLK974
Description
permet de charger une page html externe(web) et sélectionner 
les informations dont l'on a besoin pour l'afficher dans 
un div spécifique. Permet de ne pas utliser les frames.

-----------------------------------------------------------------------------

/ ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Projet final</title>
<script type="text/javascript">

 

function envoieRequete(url,id,color1,color2,im,type)
{
     
	  
	var xhr_object = null;
	var position = id;
	if(window.XMLHttpRequest)
		 { xhr_object = new XMLHttpRequest(); }
	else if(window.ActiveXObject)
		{ xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); }
		else
			{ alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); } 
		
	
	xhr_object.open("GET", url, true);
	xhr_object.onreadystatechange = function(){
		if ( xhr_object.readyState == 4 )
		{
		// j'affiche dans la DIV spécifiées le contenu retourné par le fichier
			document.getElementById(position).innerHTML = xhr_object.responseText;
		
			
		// 2009/11/30 FLK974  Met l'adresse absolue des images et cache info inutile	
		if(type=='meteo'){
			menuC1 = document.getElementById('cityframe');
			menuC1.style.display="none";
			
			if(document.getElementById('ecwarnbar')){
			menuC1 = document.getElementById('ecwarnbar');
			menuC1.style.display="none";
			node = document.getElementsByTagName('p');
			node.item(2).id="degre";
			}else{node = document.getElementsByTagName('p');
			node.item(0).id="degre";}
			
			if(document.getElementById('stormwatch_bar_fr')){
			menuC1 = document.getElementById('stormwatch_bar_fr');
			menuC1.style.display="none";
			node = document.getElementsByTagName('p');
			node.item(5).id="degre";
			}else{node = document.getElementsByTagName('p');
			node.item(0).id="degre";}
			
			
			
			menuC1 = document.getElementById('celc_on');
			menuC1.style.display="none";
			menuC1 = document.getElementById('fahr');
			menuC1.style.display="none";
			
			node = document.getElementsByTagName('a');
			for(i=0;i<node.length;i++){
				node.item(i).removeAttribute('href');
			}
			
			
			
			node = document.getElementsByTagName('table');
			node.item(0).style.display="none";
			
			node = document.getElementsByTagName('img');
			for(i=0;i<node.length;i++){
				node.item(i).src = "http://www.meteomedia.com/"+node.item(i).src.substr(22,40);
			}
			
			
			
			// 2009/11/1 FLK974  ajoute un balise	
			vi = document.createElement('input');
			vi.setAttribute('onclick', 'changePage1()');
			vi.setAttribute('type', 'button');
			vi.setAttribute('value', 'Cliquer ICI');
			vi.setAttribute('id', 'flk');  
			document.getElementsByTagName('h2').item(1).appendChild(vi);
			
			
			} 
			// 2009/12/4 FLK974  mise en forme des informations
		if(type=='bank'){
			node = document.getElementsByTagName('p');
			node.item(2).style.display="none";
			node.item(3).style.display="none";
			
			node = document.getElementsByTagName('h3');
			node.item(0).style.display="none";
			
			
			
			node = document.getElementsByTagName('caption');
			node.item(0).style.display="none";
			node = document.getElementsByTagName('tr');
			node.item(3).style.display="none";
			node.item(2).className="cellPaire";
			node.item(10).className="cellPaire";
			for(i=5;i<10;i++){
				node.item(i).style.display="none";
			}
			for(i=11;i<node.length-1;i++){
				node.item(i).style.display="none";
			}
			
			document.getElementById('t2r25c1').innerHTML ="Suisse <span id='JM'>for J.L</span>";
			
		}
			l=0;
			changeOpac(position);
		}
		
		
	}
	// dans le cas du get
	document.getElementById(position).innerHTML ="<div id='tmp'><blink>chargement</blink></div>";
	xhr_object.send(null);
	// 2009/11/30 FLK974 change aparence du banniere et barre menu
	 if(document.getElementById)
    	{
			menuC1 = document.getElementById('header');
			menuC1.style.backgroundColor=color1;
			menuC1.style.backgroundImage=im;
			menuC1.style.backgroundRepeat="no-repeat";
			menuC1 = document.getElementById('gauche');
			menuC1.style.backgroundColor=color2;
			node = document.getElementsByTagName('body');
			node.item(0).id=type;
			node = document.getElementsByTagName('a');
			for(i=0;i<node.length;i++){
				node.item(i).removeAttribute('href');
			}
			
			}
}

	var f=0;
	// 2009/11/30 FLK974  rendre visible le tableau météo
	function changePage1()
  	{
		
	   if(document.getElementById)
    	{
			f++;
			if(f==1){
			node = document.getElementsByTagName('table');
			node.item(0).style.display="";}
			else {
				node = document.getElementsByTagName('table');
				node.item(0).style.display="none";
				f=0;
			}
				
		}
	}
	
	var l=0;
	// 2009/11/30 FLK974  effet apparition
function changeOpac(id) {
	
	l=l+2;
    var object = document.getElementById(id).style;
    object.opacity = (l / 100);
    object.MozOpacity = (l / 100);
    object.KhtmlOpacity = (l / 100);
    object.filter = "alpha(opacity=" + l + ")";
	actif = setTimeout("changeOpac('"+id+"')", 20);
	if(l==100)clearTimeout(actif);
}
	
</script>
<style type="text/css">
body
{
font-family:Verdana, Arial, Helvetica, sans-serif;
font-size: 0.8em;
margin:0;
padding: 0;

}
#header
{
width: 100%;	
height:15%;
background-color: #99CCCC;

}

#conteneur
{
	 position: absolute;
 width: 100%;
 height:100%; 
background-color:#FFF;
}

#centre
{
	position: absolute;
 width: 900px;
 height:400px; 

margin-left: 170px;
}

#gauche
{
	 position: absolute;
 height: 85%; 
width: 10%; 
background-color: #00CED1;
}

/* 2009/11/30 FLK974 mise en forme Meteo */

#tmp{ color:#F00; font-size:48px; text-align:center;}
#meteo table{ border:double #003876; width:500px; margin-left:10%;}
#meteo th{ background-color:#dfecfa; color:#003876;}
#meteo td {color:#003876;  }
#meteo h1 {color:#003876;}
#meteo em {color:#2981da;}
#meteo h2 {color:#003876;}	
#meteo span{ color:#666666;font-size:10px;}
#warntitle {display:none;}
#warnmsg {display:none;}

.cond{ font-weight:bold; font-size:10px; }
 .desc{color:#666666;}

#condicon{	margin-left:5%;}
#flk{	position:relative; left:10px; top:-8px;}
#degre { color:#003876;font-size:48px;margin-left:170px;margin-top:-70px;}

#tempunit{	color:#003876; margin-left:220px; font-size:28px; margin-top:-90px}
#conddesc{	 color:#003876; font-size:15px;position:relative;  top:-20px;left:130px}

#obs_lists{ position:relative; left:30%; top:-150px; color:#003876; font-size:15px;}
#meteo  ul{ list-style: none; float:left; }
#stermfx{ position:absolute; left:0px; top:200px; width:100%}

    /* 2009/11/30 FLK974 mise en forme Meteo ---------------------------------------------*/

    /* 2009/12/3 FLK974 mise en forme bank */

#bank h1 {color:#008c53;}
    .cellFondTitre th{ background-color:#008c53; color:#FFF}
.cellFondSousTitre th{ background-color:#CCC; }
.cellPaire th{ background-color:#c5fae4; }
.cellPaire td{ background-color:#c5fae4; }
#bank table{ border:double #000;}
#JM{ color:#F00; font-size:14px; }
</style>
</head>

<body>
<div id="conteneur" >

<div id="header" >
</div>
<div id="gauche">
<!-- 2009/11/26 FLK974 ajouter adresse position cojorH colorM url logoH nomStyle  -->
<a href="#"   onclick="envoieRequete('laison1.php','centre','#008c53','#e4e4e4','url(logo.gif)','bank');" ><h1>Taux</h1></a>
<!-- 2009/11/26 FLK974 ajouter adresse position cojorH colorM url logoH nomStyle  --> <br />
<a href="#"  onclick="envoieRequete('liaison.php','centre','#003e83','#ffea5e','url(logo.jpg)','meteo');"  ><h1>Méteo	</h1></a><br />

</div>
<div id="centre">

</div>
</div>
</body>
</html>

                    page laison1.php

                    <!-- 2009/12/03 FLK974 récupère le contenu du page web  -->
<?php
$ch = curl_init();
$timeout = 5; // fixée à zéro pour pas de timeout
curl_setopt ($ch, CURLOPT_URL, 'http://www.desjardins.com/fr/taux/change/tableau_cheques.jsp');
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
$file_contents = utf8_encode  ( curl_exec($ch));//récupère le contenu
curl_close($ch);
$lines = array();//crée tableau
$lines = explode("\n", $file_contents);// ajoute le contenu

// 2009/12/3 FLK974 affichage ligne par ligne
foreach($lines as $line_num => $line) {
//echo $line_num.htmlentities($line)."  <br/> "; //  2009/11/28 FLK974 pour trouver les line
    if($line_num>323  && $line_num<937   )
        echo $line;
}

?>

                    page liaison.php

                    <!-- 2009/11/27 FLK974 récupère le contenu du page web  -->
                    <?php
$ch = curl_init();
$timeout = 5; // fixée à zéro pour pas de timeout
curl_setopt ($ch, CURLOPT_URL, 'http://www.meteomedia.com/weather/CAQC0107');
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
$file_contents = utf8_encode  ( curl_exec($ch));//récupère le contenu
curl_close($ch);
$lines = array();//crée tableau
$lines = explode("\n", $file_contents);// ajoute le contenu

// 2009/11/28 FLK974 affichage ligne par ligne
foreach($lines as $line_num => $line) {
//echo $line_num.htmlentities($line)."  <br/> "; //  2009/11/28 FLK974 pour trouver les line
    if($line_num>270  && $line_num<370 || $line_num>446  && $line_num<521 || $line_num==380 )
        echo $line;
}

?>